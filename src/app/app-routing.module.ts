import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsoldateReportComponent } from './consoldate-report/consoldate-report.component';
import { DasboardComponent } from './dasboard/dasboard.component';
import { HomepageComponent } from './homepage/homepage.component';
import { UploaddocumentComponent } from './uploaddocument/uploaddocument.component';
const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'uploaddocument', component:UploaddocumentComponent },
  { path: 'view-sheets/:sheetname', component: DasboardComponent },
  {path: 'viewconsolidatereport', component: ConsoldateReportComponent },
  {path: 'home',component: HomepageComponent},
  {
    path: '**', redirectTo: '/home' ,pathMatch: 'full'
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
