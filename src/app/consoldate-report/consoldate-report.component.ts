import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { RetriveDataServiceService } from '../retrive-data-service.service';

@Component({
  selector: 'app-consoldate-report',
  templateUrl: './consoldate-report.component.html',
  styleUrls: ['./consoldate-report.component.css']
})
export class ConsoldateReportComponent implements OnInit {

  constructor(private retriveDataServiceService:RetriveDataServiceService,private _liveAnnouncer: LiveAnnouncer) { }
  dataSource:any;
  displayedColumns:any[]=[];
  data:any;
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort

  ngOnInit(): void {
    this.generateConsolidatedReport();
  }

  generateConsolidatedReport(){
      this.retriveDataServiceService.getCombinedData().subscribe(response =>{
           this.data = response.data;
           this.dataSource = new MatTableDataSource( this.data);
           this.displayedColumns = Object.keys( this.data[0]);
           this.dataSource.paginator = this.paginator;
           this.dataSource.sort = this.sort;
      })
  }

  
  announceSortChange(sortState: Sort) {

    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    console.log(this.dataSource.filter);
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  exportAsXLSX(): void {
    this.retriveDataServiceService.exportAsExcelFile(this.data, "Consolidated Report");
  }

}
