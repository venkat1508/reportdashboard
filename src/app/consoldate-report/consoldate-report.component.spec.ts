import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsoldateReportComponent } from './consoldate-report.component';

describe('ConsoldateReportComponent', () => {
  let component: ConsoldateReportComponent;
  let fixture: ComponentFixture<ConsoldateReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsoldateReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsoldateReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
