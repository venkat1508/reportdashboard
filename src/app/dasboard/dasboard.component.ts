import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ChartConfiguration, ChartData, ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { BaseChartDirective, Label } from 'ng2-charts';
import { RetriveDataServiceService } from '../retrive-data-service.service';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-dasboard',
  templateUrl: './dasboard.component.html',
  styleUrls: ['./dasboard.component.css']
})

export class DasboardComponent implements OnInit {

  constructor(private retriveDataServiceService: RetriveDataServiceService, private _liveAnnouncer: LiveAnnouncer,private activatedRoute: ActivatedRoute) { }
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  listofSheet: any[];

  public barchartData: ChartDataSets[] = [{ data: [], label: "Total Price", backgroundColor: [
    'rgb(66, 151, 161)',
    'rgb(66, 151, 161)',
    'rgb(66, 151, 161)'
    ]},{
      type: 'line',
      label: 'Total Price',
      data: [],
      fill:false,
      borderColor:'rgb(66, 151, 161)'
  }];
  public lineChartData: ChartDataSets[] = [{ data: [], label: 'Explanded'}, { data: [], label: 'New Business', }, { data: [], label: 'Renewel', }];;
  public radarChartData: ChartDataSets[] = [{ data: [], label: 'Explanded',backgroundColor:'rgb(66, 151, 161)',fill:true,borderColor:'rgb(66, 151, 161)',pointBackgroundColor:'rgb(66, 151, 161)'
 }, { data: [], label: 'New Business',backgroundColor:'rgb(125, 118, 164)',fill:true,borderColor:'rgb(125, 118, 164)',pointBackgroundColor:'rgb(125, 118, 164)' }, { data: [],
   label: 'Renewel',backgroundColor:'rgb(141, 109, 101)',fill:true,borderColor:'rgb(141, 109, 101)',pointBackgroundColor:'rgb(141, 109, 101)' }];
  public piechartData: ChartDataSets[] = [{
    data: [], backgroundColor: [
      'rgb(141, 131, 82)',
      'rgb(125, 118, 164)',
      'rgb(66, 151, 161)'
      ]
  }]
  public doughnutchartData: ChartDataSets[] = [{
    data: [],label:"Explanded", backgroundColor: [
      'rgb(255, 99, 132)',
      'rgb(54, 162, 235)',
      'rgb(255, 205, 86)']
  }, {
    data: [], label: 'New Business',backgroundColor: [
      'rgb(255, 99, 132)',
      'rgb(54, 162, 235)',
      'rgb(255, 205, 86)']
  }, {
    data: [],label: 'Renewel', backgroundColor: [
      'rgb(255, 99, 132)',
      'rgb(54, 162, 235)',
      'rgb(255, 205, 86)']
  }];
  public polarAreaChartData :ChartDataSets[] = [{ data: [], label: "Series 1",backgroundColor: [
    'rgb(141, 131, 82)',
    'rgb(125, 118, 164)',
    'rgb(66, 151, 161)'
    ] }];
  public polarchartlabels:any=[];
  public doughnutchartlables: any[] = [];

  public radarChartlabels: string[] = [];
  public piechartlables: string[] = [];
  public barchartlables: any[] = [];
  public lineChartLabels: any[] = [];
  dataSheets: any[] = [];
  businessdata:any;

  dataSource: any;
  displayedColumns: any[] = [];
  businessTypeCount:any;
  businessTypeLabels: any;
  businessTypeTotalPrices: any;
  expandedPrices: any[] = [];
  newBusiness: any[] = [];
  renewal: any[] = [];
  sheet: any = "Level-1"
  showlineChart: boolean = false;
  lineChartOverAllLabels: any[] = [];
  lineChartOverAllLData: ChartDataSets[] = [{ data: [], label: 'Price',backgroundColor:'rgb(191, 237, 242)',fill:true,borderColor:'rgb(97, 191, 202)',pointBackgroundColor:'rgb(97, 191, 202)' }];
  level2AList: any[] = [];
  level2BList: any[] = [];
  level2CList: any[] = [];
  level2DList: any[] = [];
  level2EList: any[] = [];
  level3AList: any[] = [];
  level3BList: any[] = [];
  level3CList: any[] = [];

  ngOnInit(): void {

    this.listofSheet = [];
    this.activatedRoute.params.subscribe(
      (params: Params) => (this.sheet = params["sheetname"])
    );
    // this.generatedata();
    this.sheetChange(this.sheet);
  }



  public lineChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
      labels: {
        // fontColor: "black",

        boxWidth: 12,
        padding: 5,
        usePointStyle: true,
      }
    }
  };




  getHeaders() {
    let headers: string[] = [];
    if (this.dataSheets) {
      this.dataSheets.forEach((value) => {
        Object.keys(value).forEach((key) => {
          if (!headers.find((header) => header == key)) {
            headers.push(key)
          }
        })
      })
    }
    return headers;
  }

  sheetChange(sheetChange: string) {
    this.sheet = sheetChange;
    this.retriveDataServiceService.getSheet(sheetChange).subscribe(response => {
      this.dataSheets = response.data;
      this.businessdata = response.Revenue_type
      this.dataSource = new MatTableDataSource(this.dataSheets);
      this.displayedColumns = Object.keys(this.dataSheets[0]);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.datapreparation();
      this.initializeCharts();
    })
  }

  exportAsXLSX(): void {
    this.retriveDataServiceService.exportAsExcelFile(this.dataSheets, this.sheet);
  }

  generatedata() {
    this.retriveDataServiceService.getFullData().subscribe(response => {
      this.dataSheets = response.data;
      this.dataSource = new MatTableDataSource(this.dataSheets);
      this.displayedColumns = Object.keys(this.dataSheets[0]);
      this.listofSheet = response.sheets;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.datapreparation();
      this.initializeCharts();

    })
  }

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  // charts data preparation 

  public datapreparation() {
    let expandedlist = this.businessdata['Expanded'];
        let newBusiness = this.businessdata['New Business'];
        let renewal = this.businessdata['Renewal'];
        let dataList = [];
        dataList.push(expandedlist);
        dataList.push(newBusiness);
        dataList.push(renewal);
    this.businessTypeCount=dataList;
    this.businessTypeTotalPrices = this.transform(this.dataSheets, 'Business Type', 'total_price').map(a => a['sum']);
    this.businessTypeLabels = this.transform(this.dataSheets, 'Business Type', 'total_price').map(a => a['key']);
    this.businessTypeTotalPrices = this.businessTypeTotalPrices.filter(a => a != 0);
    this.businessTypeLabels = this.businessTypeLabels.filter(a => a != '0');
    this.expandedPrices = this.dataSheets.filter(a => a['Business Type'] != 'Explanded').map(a => a.total_price);
    this.newBusiness = this.dataSheets.filter(a => a['Business Type'] != 'New Business').map(a => a.total_price);
    this.renewal = this.dataSheets.filter(a => a['Business Type'] != 'Renewel').map(a => a.total_price);
    this.showlineChart = false;
    if (this.sheet == "Revenue by client") {
      this.level2AList = this.transform(this.dataSheets, 'Customer Name', 'total_price');
      this.setLineChartForUpLevels(this.level2AList);
      this.showlineChart = true;
    }
    if (this.sheet == "Revenue By Practice") {
      this.level2BList = this.transform(this.dataSheets, 'Product Class', 'total_price');
      this.setLineChartForUpLevels(this.level2BList);
      this.showlineChart = true;
    }
    if (this.sheet == "Revenue By Segment") {
      this.level2CList = this.transform(this.dataSheets, 'segment', 'total_price');
      this.setLineChartForUpLevels(this.level2CList);
      this.showlineChart = true;
    }
    if (this.sheet == "Revenue By Entity") {
      this.level2DList = this.transform(this.dataSheets, 'Product', 'total_price');
      this.setLineChartForUpLevels(this.level2DList);
      this.showlineChart = true;
    }
    // if (this.sheet == "Level-2E") {
    //   this.level2EList = this.transform(this.dataSheets, 'Product', 'total_price');
    //   this.setLineChartForUpLevels(this.level2EList);
    //   this.showlineChart = true;
    // }
    if (this.sheet == "Revenue Type-Client-Practice") {
      this.level3AList = this.transform(this.dataSheets, 'Customer Name', 'total_price');
      this.setLineChartForUpLevels(this.level3AList);
      this.showlineChart = true;
    }
    if (this.sheet == "Revenue Type-Entity-Client") {
      this.level3BList = this.transform(this.dataSheets, 'Customer Name', 'total_price');
      this.setLineChartForUpLevels(this.level3BList);
      this.showlineChart = true;
    }
    if (this.sheet == "Revenue Type-Practice-Segment") {
      this.level3CList = this.transform(this.dataSheets, 'Product Class', 'total_price');
      this.setLineChartForUpLevels(this.level3CList);
      this.showlineChart = true;
    }

    console.log(this.level2AList)
  }

  transform(collection: object[], property: string, sum: string): object[] {
    if (!collection) { return null; }

    const groupedCollection = collection.reduce((previous, current) => {
      if (!previous[current[property]]) {
        previous[current[property]] = [current];
      } else {
        previous[current[property]].push(current);
      }

      return previous;
    }, {});
    return Object.keys(groupedCollection).map(key => ({
      key,
      sum: groupedCollection[key].reduce((a, b) => a + parseInt(b[sum]), 0),
      value: groupedCollection[key]
    }));
  }


  announceSortChange(sortState: Sort) {

    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  // charts prepare
  initializeCharts() {
    this.setPieChartDatas();
    this.setBarChartDatas();
    this.setLineChartDatas();
    this.setRadarChartDatas();
    this.setdoughnutChart();
    this.setPolarChart();
  }



  //  bar Chart Preparation
  setBarChartDatas() {
    this.barchartData[0].data =this.businessTypeCount;
    this.barchartData[1].data=this.businessTypeCount;
    this.barchartlables = this.businessTypeLabels;
  }

  // pie Chart Preparation
  setPieChartDatas() {
    this.piechartData[0].data = this.businessTypeTotalPrices;
    this.piechartlables = this.businessTypeLabels;
  }

  setLineChartDatas() {
    for (let i = 0; i < 3; i++) {
      if (i == 0)
        this.lineChartData[i].data = this.expandedPrices;
      if (i == 1)
        this.lineChartData[i].data = this.newBusiness;
      if (i == 2)
        this.lineChartData[i].data = this.renewal;
    }
    let listOfPoints = [];
    listOfPoints.push(this.expandedPrices);
    listOfPoints.push(this.newBusiness);
    listOfPoints.push(this.renewal);

    this.lineChartLabels = this.businessTypeTotalPrices;
  }

  setRadarChartDatas() {
    this.radarChartlabels = this.businessTypeLabels;
    for (let i = 0; i < 3; i++) {
      if (i == 0)
        this.radarChartData[i].data = this.expandedPrices;
      if (i == 1)
        this.radarChartData[i].data = this.newBusiness;
      if (i == 2)
        this.radarChartData[i].data = this.renewal;
    }
  }

  setdoughnutChart() {
    this.doughnutchartlables = this.businessTypeLabels;
    for (let i = 0; i < 3; i++) {
      if (i == 0)
        this.doughnutchartData[i].data = this.expandedPrices.filter(a => a>0);
      if (i == 1)
        this.doughnutchartData[i].data = this.newBusiness.filter(a => a>0);
      if (i == 2)
        this.doughnutchartData[i].data = this.renewal.filter(a => a>0);
    }
    console.log(this.doughnutchartData);
  }

  setLineChartForUpLevels(datamap) {
    this.lineChartOverAllLData[0].data = datamap.map(a => a['sum']);
    this.lineChartOverAllLabels = datamap.map(a => a['key'])
  }

  setPolarChart(){
    this.polarAreaChartData[0].data = this.businessTypeTotalPrices;
    this.polarchartlabels = this.businessTypeLabels;
  }




}
