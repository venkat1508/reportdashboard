import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoughnutChrtComponent } from './doughnut-chrt.component';

describe('DoughnutChrtComponent', () => {
  let component: DoughnutChrtComponent;
  let fixture: ComponentFixture<DoughnutChrtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoughnutChrtComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoughnutChrtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
