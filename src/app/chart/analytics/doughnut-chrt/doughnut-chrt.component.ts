import { Component, Input, OnInit } from '@angular/core';
import { ChartDataSets } from 'chart.js';

@Component({
  selector: 'app-doughnut-chrt',
  templateUrl: './doughnut-chrt.component.html',
  styleUrls: ['./doughnut-chrt.component.css']
})
export class DoughnutChrtComponent implements OnInit {

  constructor() { }

  @Input()
  public doughnutchartlables = [];

  @Input()
  public doughnutchartData: ChartDataSets[] = [{ data: []}, { data: []}, { data: []}];
  
  public doughnutchartType = 'doughnut';

  ngOnInit(): void {
  }

 

}
