import { Component, Input, OnInit } from '@angular/core';
import { ChartDataSets, ChartType } from 'chart.js';

@Component({
  selector: 'app-polarchart',
  templateUrl: './polarchart.component.html',
  styleUrls: ['./polarchart.component.css']
})
export class PolarchartComponent implements OnInit {

  constructor() { }

  public polarAreaLegend = true;
  
  @Input()
  public polarchartlabels:any = []

  public polarAreaChartType: ChartType = 'polarArea';

  @Input()
  public polarAreaChartData: ChartDataSets[] = [{ data: [], label: 'Series 1', }]

  ngOnInit(): void {
  }

}
