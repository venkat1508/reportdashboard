import { Component, Input, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-line',
  templateUrl: './line.component.html',
  styleUrls: ['./line.component.css']
})
export class LineComponent implements OnInit {

  constructor() { }

  public lineChartType ="line";

  @Input()
  public lineChartData: ChartDataSets[] = [{ data: [], label: 'Explanded' }, { data: [], label: 'New Business'}, { data: [], label: 'Renewel'}];

  @Input()
  public lineChartLabels: Label[] = [];

  ngOnInit(): void {
  }

  public lineChartOptions: ChartOptions = {
    responsive: true,
    animation: {
       
      duration: 3000,
      easing: 'linear',
      animateScale:true,
      animateRotate:true,
      
    
  }
    }
  };


