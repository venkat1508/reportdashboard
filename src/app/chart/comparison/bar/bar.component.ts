import { Component, Input, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';

@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.css']
})
export class BarComponent implements OnInit {

  
  @Input()
  public barchartData: ChartDataSets[] = [{ data: [] ,label: "Total Price" }];

  @Input()
  public barchartlables: string[] = [];
  public barchartType: string = 'bar';

  constructor() { }

  ngOnInit(): void {

  }
  public chartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };

}
