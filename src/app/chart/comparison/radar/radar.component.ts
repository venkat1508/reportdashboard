import { Component, Input, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';

@Component({
  selector: 'app-radar',
  templateUrl: './radar.component.html',
  styleUrls: ['./radar.component.css']
})
export class RadarComponent implements OnInit {

  constructor() { }

  public radarChartType: string = 'radar';

  @Input()
  public radarChartlabels: string[] = [];
  
  @Input()
  public radarChartData: ChartDataSets[] = [{ data: [], label: 'Explanded', }, { data: [], label: 'New Business', }, { data: [], label: 'Renewel', }];

  ngOnInit(): void {
  }

  public radarChartOptions: ChartOptions = {
    responsive: true,
  };

}
