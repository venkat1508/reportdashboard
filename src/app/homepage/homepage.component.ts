import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Chart, ChartOptions } from 'chart.js';
import { RetriveDataServiceService } from '../retrive-data-service.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  constructor(private retriveDataServiceService: RetriveDataServiceService, private route: Router) { }



  sheets: any[] = [];
  ngOnInit(): void {


    this.retriveDataServiceService.getFullData().subscribe(response => {
      this.sheets = response?.Revenue_type;
      this.ionViewDidLoad()


    })
  }

  viewconsolidatedreport(){
    this.route.navigateByUrl('/viewconsolidatereport');
  }

  viewsheet(sheet) {
    this.route.navigateByUrl('/view-sheets/' + sheet.name);
  }


  ionViewDidLoad() {
    setTimeout(() => {


      var data = {
        labels: [],
        datasets: [
          {
            data: [],
            backgroundColor: 'rgb(191, 237, 242)', fill: true, borderColor: 'rgb(97, 191, 202)', pointBackgroundColor: 'rgb(97, 191, 202)'
          }

        ],
      };

      //options
      var option: ChartOptions = {
        responsive: true,
        title: {
          display: false,
          position: 'top',
        },
        legend: {
          display: false

        }, animation: {

          duration: 1000,
          easing: 'easeInQuad',
          animateScale: true,
          animateRotate: true,


        },
        scales: {
          yAxes: [{
            display: false,
           
          }],
          xAxes: [{
            display: false,
           
          }]

        },

      };
      for (let i = 0; i < this.sheets.length; i++) {
        let ctx: any = document.getElementById(this.sheets[i].name) as HTMLElement;
        let expandedlist = this.sheets[i]['Expanded'];
        let newBusiness = this.sheets[i]['New Business'];
        let renewal = this.sheets[i]['Renewal'];
        let dataList = [];
        dataList.push(expandedlist);
        dataList.push(newBusiness);
        dataList.push(renewal);
        data.datasets[0].data = dataList;
        data.labels = ["Expanded", "New Bussiness", "Renewal"]


        var chart = new Chart(ctx, {
          options: option,
          type: 'line',
          data: data
        },);

      }
    }, 1000);
  }




}
