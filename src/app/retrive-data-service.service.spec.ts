import { TestBed } from '@angular/core/testing';

import { RetriveDataServiceService } from './retrive-data-service.service';

describe('RetriveDataServiceService', () => {
  let service: RetriveDataServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RetriveDataServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
