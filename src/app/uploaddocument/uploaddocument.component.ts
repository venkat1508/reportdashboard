import { Component, OnInit } from '@angular/core';
import {  Route, Router } from '@angular/router';

@Component({
  selector: 'app-uploaddocument',
  templateUrl: './uploaddocument.component.html',
  styleUrls: ['./uploaddocument.component.css']
})
export class UploaddocumentComponent implements OnInit {

  constructor(
    private route:Router
  ) { }

  fileName:any

  ngOnInit(): void {
  }
  
  onFileSelected(event) {

    const file:File = event.target.files[0];

    if (file) {

        this.fileName = file.name;

        const formData = new FormData();

        formData.append("thumbnail", file);

        
    }
    
}
generate(){
      this.route.navigateByUrl('/view-sheets');
}
}
