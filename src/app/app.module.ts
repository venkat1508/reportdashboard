import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DasboardComponent } from './dasboard/dasboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import { RetriveDataServiceService } from './retrive-data-service.service';
import { HttpClientModule } from '@angular/common/http';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ChartsModule } from 'ng2-charts';
import { UploaddocumentComponent } from './uploaddocument/uploaddocument.component';
import {MatIconModule} from '@angular/material/icon';
import { BarComponent } from './chart/comparison/bar/bar.component';
import { RadarComponent } from './chart/comparison/radar/radar.component';
import { TreemapComponent } from './chart/comparison/treemap/treemap.component';
import { ScatterComponent } from './chart/analytics/scatter/scatter.component';
import { ViolinComponent } from './chart/analytics/violin/violin.component';
import { HeatmapComponent } from './chart/analytics/heatmap/heatmap.component';
import { LineComponent } from './chart/analytics/line/line.component';
import { PieChartComponent } from './chart/analytics/pie-chart/pie-chart.component';
import { DoughnutChrtComponent } from './chart/analytics/doughnut-chrt/doughnut-chrt.component';
import { PolarchartComponent } from './chart/analytics/polarchart/polarchart.component';
import { HomepageComponent } from './homepage/homepage.component';
import { OverviewComponent } from './overview/overview.component';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { ConsoldateReportComponent } from './consoldate-report/consoldate-report.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';


@NgModule({
  declarations: [
    AppComponent,
    DasboardComponent,
    UploaddocumentComponent,
    BarComponent,
    RadarComponent,
    TreemapComponent,
    ScatterComponent,
    ViolinComponent,
    HeatmapComponent,
    LineComponent,
    PieChartComponent,
    DoughnutChrtComponent,
    PolarchartComponent,
    HomepageComponent,
    OverviewComponent,
    ConsoldateReportComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatToolbarModule,
    ChartsModule,
    MatIconModule,
    NgHttpLoaderModule.forRoot(),
    FormsModule,
    MatInputModule

   
  ],
  providers: [RetriveDataServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
